package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	router "gitlab.com/branch-list/clients/master/routers"
)

func main() {

	portMain := os.Getenv("PORT_CLIENT_MASTER")

	serverMain := &http.Server{
		Addr:           portMain,
		Handler:        router.Router(),
		ReadTimeout:    360 * time.Second,
		WriteTimeout:   360 * time.Second,
		MaxHeaderBytes: 1 << 60,
	}

	log.Println("RUN SERVER IN ", portMain, " HIT : ", time.Now())
	fmt.Println("PORT MAIN", portMain)
	serverMain.ListenAndServe()
}
