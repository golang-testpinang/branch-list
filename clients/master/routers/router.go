package routers

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	handler "gitlab.com/branch-list/clients/master/handlers"
	auth "gitlab.com/branch-list/helpers/jwt"
)

func Router() http.Handler {
	// Force log's color
	gin.DisableConsoleColor()

	// Logging to a file.
	t := time.Now()
	formatDate := t.Format("20060102")
	logJoin := []string{"logs", "/", "clients", "/", "master", "/", "log", "-", formatDate, ".log"}
	logFile := strings.Join(logJoin, "")
	f, _ := os.OpenFile(logFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0755)

	// Use the following code if you need to write the logs to file and console at the same time.
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)
	router := gin.New()
	router.Use(gin.LoggerWithFormatter(func(param gin.LogFormatterParams) string {

		return fmt.Sprintf("%s - [%s] \"%s %s %s %d %s \"%s\" %s\"\n",
			param.ClientIP,
			param.TimeStamp.Format(time.RFC1123),
			param.Method,
			param.Path,
			param.Request.Proto,
			param.StatusCode,
			param.Latency,
			param.Request.UserAgent(),
			param.ErrorMessage,
		)
	}))

	masterMasterEndpoint := router.Group("/v/1/master")
	{
		masterMasterEndpoint.Use(auth.TokenAuthentication())
		{
			masterMasterEndpoint.POST("/branch-list", handler.HandlerBranchList)
		}

	}
	return router
}
