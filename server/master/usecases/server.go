package usecases

import (
	"context"
	"encoding/json"
	"io"
	"log"
	"os"
	"runtime"
	"strings"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	config "gitlab.com/branch-list/configs"
	helpers "gitlab.com/branch-list/helpers/utils"
	model "gitlab.com/branch-list/models"
	pb "gitlab.com/branch-list/proto/master"
	"google.golang.org/grpc"
)

type grpcServer struct {
	repository model.Repositories
	pb.UnimplementedMasterServiceServer
}

func NewRPCServer(repository model.Repositories) *grpc.Server {
	srv := grpcServer{
		repository: repository,
	}

	gsrv := grpc.NewServer(
		grpc.MaxRecvMsgSize(1024*1024*20),
		grpc.MaxSendMsgSize(1024*1024*20),
	)
	pb.RegisterMasterServiceServer(gsrv, &srv)
	return gsrv
}

func (server *grpcServer) MasterBranchList(ctx context.Context, request *pb.BranchListRequest) (*pb.BranchListResponse, error) {
	t := time.Now()
	formatDate := t.Format("20060102")
	logJoin := []string{"logs", "/", "server", "/", "master", "/", "log", "-", formatDate, ".log"}
	logFile := strings.Join(logJoin, "")
	_, err := os.Stat(logFile)

	//check exist file log
	f, _ := os.OpenFile(logFile, os.O_RDWR|os.O_APPEND, 0755)
	if os.IsNotExist(err) {
		f, _ = os.OpenFile(logFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0755)

	}
	// Use the following code if you need to write the logs to file and console at the same time.
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)

	log.SetOutput(gin.DefaultWriter)

	response := &pb.BranchListResponse{}
	userId := request.GetUserId()
	textSearch := request.GetTextSearch()
	startIndex := request.GetStartIndex()
	recordCount := request.GetRecordCount()

	params := model.BranchParams{
		StartIndex:  startIndex,
		RecordCount: recordCount,
		UserId:      userId,
		TextSearch:  textSearch,
	}

	var responseResult model.BranchResponse
	var dataResult []model.BranchMapping
	responseResult, dataResult = server.repository.GetBranch(params)

	log.Println("Service ** RESULT QUERY BRANCH LIST **")
	log.Println(response)

	var resultData []*pb.EmbedDataBranchList
	for _, vData := range dataResult {
		dataRow := pb.EmbedDataBranchList{
			BranchCode:               vData.Code,
			BranchCorrespondenceCode: vData.CorrespondanceCode,
			BranchName:               vData.Name,
			BranchLevel:              vData.Level,
			BranchHead:               vData.Head,
		}

		resultData = append(resultData, &dataRow)
	}

	//response success
	response = &pb.BranchListResponse{
		Status:               200,
		Message:              responseResult.Message,
		MessageLocal:         responseResult.MessageLocal,
		EmbedDataBranchLists: resultData,
	}

	//PROCESS TO LOGGING CLOUD
	{
		requestData := map[string]interface{}{
			"userId":      request.GetUserId(),
			"textSearch":  request.GetTextSearch(),
			"startIndex":  request.GetStartIndex(),
			"recordCount": request.GetRecordCount(),
		}

		dataRequest, err := json.Marshal(requestData)
		if err != nil {
			log.Println("Service ** ERROR LOGGING CLOUD : ", err)

			response = &pb.BranchListResponse{
				Status:       200,
				Message:      responseResult.Message,
				MessageLocal: responseResult.MessageLocal,
			}

			return response, nil
		}

		log.Println("Service ** DATA REQUEST**")
		log.Println(string(dataRequest))

		//DATA RESPONSE
		strStatusCode, _ := helpers.IntString(200)
		responseData := map[string]interface{}{
			"statusCode":   strStatusCode,
			"responseData": response,
		}

		dataResponse, err := json.Marshal(responseData)
		if err != nil {
			log.Println("Service ** ERROR LOGGING CLOUD : ", err)

			response = &pb.BranchListResponse{
				Status:       200,
				Message:      responseResult.Message,
				MessageLocal: responseResult.MessageLocal,
			}

			return response, nil
		}

		log.Println("Service ** DATA RESPONSE**")
		log.Println(string(dataResponse))

		runtime.GOMAXPROCS(1)
		var wg sync.WaitGroup

		dLogStatus := make(chan int, 2)
		dLogMessage := make(chan string, 2)

		wg.Add(1)
		go func() {
			dataLog := config.V1LoggingCloudPubSub{
				Status:       "200",
				Endpoint:     "v/1/master/branch-list",
				UserId:       request.GetUserId(),
				ActionDate:   time.Now().Format("2006-01-02 15:04:05.000"),
				Description:  "master-logging",
				DataRequest:  string(dataRequest),
				DataResponse: string(dataResponse),
			}

			logData, _ := json.Marshal(dataLog)
			logJson := string(logData)

			logStatus, logMessage := helpers.LoggingCloudPubSubV1(&wg, logJson, "master-cloud-log")
			dLogStatus <- logStatus
			dLogMessage <- logMessage
		}()

		var statusLog = <-dLogStatus
		var messageLog = <-dLogMessage
		log.Println("Service ** RESULT SERVICE PROCESS LOGGING CLOUD **")
		log.Println(statusLog)
		log.Println(messageLog)
	}
	return response, nil
}
