package helpers

import (
	"context"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"cloud.google.com/go/storage"
	"google.golang.org/api/option"

	Config "gitlab.com/branch-list/configs"
)

func ReadFileToCloud(bucketPath string) (status int, stringPhoto []byte, mimeType string, errorMessage string) {
	var imgByte []byte

	var bucket, _ = Config.MediaBucket() //bucket name
	var appName, _ = Config.AppEnv()     //app name

	if appName != "PRODUCTION" {
		bucketPath = strings.ToLower(appName) + "/" + bucketPath
	}

	log.Println("APP ENV : ", appName)
	log.Println("Bucket Path : ", bucketPath)

	object := bucketPath //bucket path
	credentials := os.Getenv("GOOGLE_APPLICATION_PIKRO_CREDENTIALS")

	ctx := context.Background()
	client, err := storage.NewClient(ctx, option.WithCredentialsFile(credentials))
	if err != nil {
		log.Println("-- Helpers -> storage.NewClient: %v", err)

		return 400, imgByte, "", err.Error()
	}
	defer client.Close()

	rc, err := client.Bucket(bucket).Object(object).NewReader(ctx)
	if err != nil {
		log.Println("-- Helpers -> io.Copy: %v", err)

		return 400, imgByte, "", err.Error()
	}

	fileString, err := ioutil.ReadAll(rc)
	rc.Close()
	if err != nil {
		log.Println("-- Helpers -> rc.Close: %v", err)

		return 400, imgByte, "", err.Error()
	}

	mimeTypeFile := http.DetectContentType(fileString)

	return 200, fileString, mimeTypeFile, ""

}
