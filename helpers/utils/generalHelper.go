package helpers

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"fmt"
	"log"
	"math/rand"
	"strconv"
	"strings"
	"time"

	config "gitlab.com/branch-list/configs"
)

func StringInt(text string) (result int, status int) {
	result, _ = strconv.Atoi(text)

	//fmt.Println("---STRING TO INT---")
	//fmt.Println(result)
	//return

	if result != 0 {
		return result, 200
	}

	return 0, 400
}

func FloatString(text float64) (result string, status int) {
	result = fmt.Sprintf("%.2f", text)

	//fmt.Println("---FLOAT TO STRING---")
	//fmt.Println(result)
	//return

	if result != "" {
		return result, 200
	}

	return "", 400
}

func StringFloat64(text string) (result float64, status int) {
	result, _ = strconv.ParseFloat(text, 64)

	//fmt.Println("---STRING TO INT---")
	//fmt.Println(result)
	//return

	if result != 0 {
		return result, 200
	}

	return 0, 400
}

func IntString(text int) (result string, status int) {
	result = strconv.Itoa(text)

	//fmt.Println("---STRING TO INT---")
	//fmt.Println(result)
	//return

	if result != "" {
		return result, 200
	}

	return "", 400
}

func FileSize(text string) (result int, status int) {
	l := len(text)

	// count how many trailing '=' there are (if any)
	eq := 0
	if l >= 2 {
		if text[l-1] == '=' {
			eq++
		}
		if text[l-2] == '=' {
			eq++
		}

		l -= eq
	}

	result = (l*3 - eq) / 4

	return result, 200
}

func GcmDecode(text string) (result string) {
	secretKey, _ := config.GCMSecretKey()
	nonce, _ := config.GCMNonce()

	cipherText, _ := base64.StdEncoding.DecodeString(text)

	// remove 0 - 11 index (nonce)
	clearText := cipherText[12:len(cipherText)]

	secretKeyByte := []byte(secretKey)
	block, _ := aes.NewCipher(secretKeyByte)

	nonceByte := []byte(nonce)

	log.Println("CIPHER TEXT : ", text)
	log.Println("CIPHER BYTE : ", cipherText)
	log.Println("SECRET KEY : ", secretKey)
	log.Println("SECRET KEY BYTE : ", secretKeyByte)
	log.Println("NONCE : ", nonce)
	log.Println("NONCE BYTE : ", nonceByte)

	aes, _ := cipher.NewGCM(block)

	plainText, _ := aes.Open(nil, nonceByte, clearText, nil)

	result = string(plainText)

	return result
}

func ArrStringInt(arr []string) (result []int, status int) {
	for i := range arr {
		intArr, _ := StringInt(arr[i])
		result = append(result, intArr)
	}

	return result, 200
}

func ArrStringFloat64(arr []string) (result []float64, status int) {
	for i := range arr {
		floatArr, _ := StringFloat64(arr[i])
		result = append(result, floatArr)
	}

	return result, 200
}

func StrSplit(str string) (result []string, statuts int) {
	strTrim := strings.Trim(str, " ")
	strSplit := strings.Split(strTrim, " ")
	result = strSplit

	return result, 200
}

func StrSplitIsntallmentPattern(str []string) (number []int, dayMonth []string, statuts int) {
	for _, v := range str {
		strTrim := strings.Trim(v, " ")
		strSplit := strings.Split(strTrim, " ")

		if len(strSplit) < 2 {
			return nil, nil, 500
		}

		resNumber, _ := StringInt(strSplit[0])
		number = append(number, resNumber)

		dayMonthLower, _ := StrToLower(strSplit[1])
		resDayMonth := dayMonthLower
		dayMonth = append(dayMonth, resDayMonth)

	}

	return number, dayMonth, 200
}

func StrToUpper(str string) (result string, statuts int) {
	strUpper := strings.ToUpper(str)
	result = strUpper

	return result, 200
}

func StrToLower(str string) (result string, statuts int) {
	strLower := strings.ToLower(str)
	result = strLower

	return result, 200
}

func GenerateCode(text string, maxRand int) (result string) {
	// Go program to generate random characters from the string
	rand.Seed(time.Now().UnixNano())
	//STRINGS
	charset := "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

	splitText := strings.Split(text, " ")
	log.Println("SPLIT : ", splitText)

	strIntialText := ""
	switch len(splitText) {
	case 1:
		switch len(splitText[0]) {
		case 1:
			strIntialText += string(splitText[0][0])
			strIntialText += string(charset[rand.Intn(len(charset))])
			strIntialText += string(charset[rand.Intn(len(charset))])
		case 2:
			strIntialText += string(splitText[0][0])
			strIntialText += string(splitText[0][1])
			strIntialText += string(charset[rand.Intn(len(charset))])
		default:
			strIntialText += string(splitText[0][0])
			strIntialText += string(splitText[0][1])
			strIntialText += string(splitText[0][2])
		}

	case 2:
		for i := 0; i < len(splitText); i++ {
			if i == 0 {
				strIntialText += string(splitText[i][0])
			} else {
				// strIntialText += string(splitText[i][0])
				// strIntialText += string(splitText[i][1])
				switch len(splitText[i]) {
				case 1:
					strIntialText += string(splitText[i][0])
					strIntialText += string(charset[rand.Intn(len(charset))])
				default:
					strIntialText += string(splitText[i][0])
					strIntialText += string(splitText[i][1])
				}
			}
		}

	case 3:
		for i := 0; i < len(splitText); i++ {
			strIntialText += string(splitText[i][0])
		}

	default:
		for i := 0; i < 3; i++ {
			strIntialText += string(splitText[i][0])
		}
	}

	log.Println("STRING INITIAL : ", strIntialText)

	initialText := strings.ToUpper(strIntialText)
	log.Println("INITIAL : ", initialText)

	randInt := rand.Intn(maxRand)
	strRand, _ := IntString(randInt)
	log.Println("RAND : ", randInt)

	result = initialText
	result += "-"
	result += string(strRand)
	log.Println("GENERATE CODE RESULT : ", result)

	return result
}
