package helpers

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	config "gitlab.com/branch-list/configs"
	ct "gitlab.com/branch-list/helpers/constants"
)

type V1BranchDetailCurlParams struct {
	BranchCode string
}

type V1BranchDetailCurlResponse struct {
	Status       int
	Message      string
	MessageLocal string
}

type V1BranchDetailCurlMapping struct {
	StatusCode        string                  `json:"statusCode"`
	StatusDescription string                  `json:"statusDescription"`
	Data              V1DataEmbedBranchDetail `json:"data"`
}

type V1DataEmbedBranchDetail struct {
	BranchCode           string `json:"branchCode"`
	BranchCorrespondCode string `json:"branchCorrespondCode"`
	BranchName           string `json:"branchName"`
	BranchLevel          string `json:"branchLevel"`
	BranchAddress        string `json:"branchAddress"`
	BranchPhone          string `json:"branchPhone"`
	BranchHead           string `json:"branchHead"`
}

func CurlBranchDetailV1(params V1BranchDetailCurlParams) (response V1BranchDetailCurlResponse, data V1BranchDetailCurlMapping) {
	t := time.Now()
	formatDate := t.Format("20060102")
	logJoin := []string{"logs", "/", "log", "-", formatDate, ".log"}
	logFile := strings.Join(logJoin, "")
	_, err := os.Stat(logFile)

	//check exist file log
	f, _ := os.OpenFile(logFile, os.O_RDWR|os.O_APPEND, 0755)
	if os.IsNotExist(err) {
		f, _ = os.OpenFile(logFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0755)

	}
	// Use the following code if you need to write the logs to file and console at the same time.
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)

	log.SetOutput(gin.DefaultWriter)

	// log.Println("DISINI")
	firstEndpoint, _ := config.APIDataAgri()
	queryStringJoin := []string{firstEndpoint, "/api/digiagri/branch/inquiry/", params.BranchCode}
	endpoint := strings.Join(queryStringJoin, "")

	log.Println("--ENDPOINT API BRANCH DETAIL--")
	log.Println(endpoint)

	transCfg := ct.TransportConfig

	client := &http.Client{
		Timeout:   time.Duration(25 * time.Second),
		Transport: transCfg,
	}

	r, err := http.NewRequest("GET", endpoint, nil) // URL-encoded payload

	//ERROR REQUEST
	if err != nil {
		log.Println("Helper -- ERROR REQUEST CURL BRANCH DETAIL ---")
		log.Println("Helper -- ERROR REQUEST CURL BRANCH DETAIL : ", err.Error())

		joinString := []string{"Status Code : ", "-", " | Error : ", err.Error()}
		erorrMessage := strings.Join(joinString, "")

		response := V1BranchDetailCurlResponse{
			Status:       500,
			Message:      ct.ErorrGeneralMessage,
			MessageLocal: erorrMessage,
		}

		return response, data
	}

	r.Header.Set(ct.CURLHeaderContentType, ct.CURLHeaderContentTypeValue)
	r.Header.Set(ct.CURLHeaderCacheControl, ct.CURLHeaderCacheControlValue)
	// r.SetBasicAuth(usernameSSO, passwordSSO)
	curlResponse, err := client.Do(r)

	//ERROR CURL
	if err != nil {
		log.Println("Helper -- ERROR HIT API CURL BRANCH DETAIL ---")
		log.Println("Helper -- ERROR HIT API CURL BRANCH DETAIL : ", err.Error())

		joinString := []string{"Status Code : ", "-", " | Error : ", err.Error()}
		erorrMessage := strings.Join(joinString, "")

		response := V1BranchDetailCurlResponse{
			Status:       500,
			Message:      ct.ErorrGeneralMessage,
			MessageLocal: erorrMessage,
		}

		return response, data
	}

	defer curlResponse.Body.Close()

	log.Println("Helper -- RESPONSE STATUS CURL BRANCH DETAIL : ", curlResponse.Status)
	log.Println("Helper -- RESPONSE HEADERS CURL BRANCH DETAIL : ", curlResponse.Header)
	log.Println("Helper -- REQUEST URL CURL BRANCH DETAIL : ", curlResponse.Request.URL)
	log.Println("Helper -- REQUEST CONTENT LENGTH CURL BRANCH DETAIL : ", curlResponse.Request.ContentLength)

	//ERROR STATUS CODE
	if curlResponse.StatusCode != 200 {
		// log.Println("Helper --- ERROR STATUS CODE CURL BRANCH DETAIL ---")
		// log.Println("Helper --- ERROR STATUS CODE CURL BRANCH DETAIL : ", curlResponse.StatusCode)

		joinString := []string{"Status Code : ", "-", " | Error : ", err.Error()}
		erorrMessage := strings.Join(joinString, "")

		response := V1BranchDetailCurlResponse{
			Status:       500,
			Message:      ct.ErorrGeneralMessage,
			MessageLocal: erorrMessage,
		}

		return response, data
	}

	json.NewDecoder(curlResponse.Body).Decode(&data)

	// log.Println("Helper --- DATA DECODE CURL BRANCH DETAIL ---")
	// log.Println(data)

	if data.StatusCode == "99" {
		response := V1BranchDetailCurlResponse{
			Status:       401,
			Message:      data.StatusDescription,
			MessageLocal: data.StatusDescription,
		}

		return response, data
	}

	response = V1BranchDetailCurlResponse{
		Status:       200,
		Message:      "Data branch berhasil ditemukan",
		MessageLocal: "Branch Found",
	}

	return response, data
}
